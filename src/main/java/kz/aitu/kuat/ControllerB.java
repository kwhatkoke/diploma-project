package kz.aitu.kuat;

import com.martensigwart.fakeload.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

import static java.lang.Math.atan;
import static java.lang.Math.tan;

@RestController
public class ControllerB {

    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    @GetMapping("/b")
    public String serviceB() {
        // Create FakeLoad
        FakeLoad fakeload = FakeLoads.create()
                .lasting(getRandomNumber(1, 3), TimeUnit.SECONDS)
                .withCpu(8)
                .withMemory(30, MemoryUnit.MB);

        // Execute FakeLoad synchronously
        FakeLoadExecutor executor = FakeLoadExecutors.newDefaultExecutor();
        executor.execute(fakeload);

//        try {
//            Thread.sleep(getRandomNumber(3000, 50000));
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        for (int i=0; i<getRandomNumber(100000, 10000000); i++) {
//            double d = tan(atan(tan(atan(tan(atan(tan(atan(tan(atan(123456789.123456789))))))))));
//        }
        return "Service B is called from Service A";
    }

}
