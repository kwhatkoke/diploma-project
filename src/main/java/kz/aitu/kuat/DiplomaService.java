package kz.aitu.kuat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class DiplomaService {
    @Autowired
    private RestTemplate restTemplate;

    public String callB(String url) {
        return restTemplate.getForObject(
                url,
                String.class);
    }

}
