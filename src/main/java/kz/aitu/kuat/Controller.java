package kz.aitu.kuat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
class Controller {
    private static final Map<String, Long> CLIENT_REQUEST_COUNT = new HashMap<>();

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/get")
    public String HelloWorld() {
        return "Hello " + Math.random();
    }

    @GetMapping("/getCount")
    public long getCount(HttpServletRequest request) {

        String client = request.getRemoteAddr();

        if (Objects.nonNull(CLIENT_REQUEST_COUNT.get(client))) {
            long newCount = CLIENT_REQUEST_COUNT.get(client) + 1;
            CLIENT_REQUEST_COUNT.put(client, newCount);
            return newCount;
        }

        CLIENT_REQUEST_COUNT.put(client, 1L);
        return 1;
    }

    @GetMapping(path="/add")
    public @ResponseBody
    String addNewUser (@RequestParam(value = "name") String name) throws InterruptedException {
        User n = new User();
        n.setName(name);
        userRepository.save(n);
//        Thread.sleep(200);
        return String.format("Hello %s!", name);
    }

    @GetMapping(path="/get-names")
    public @ResponseBody Iterable<User> getAllUsers() {
        return userRepository.findTopByOrderByIdDesc();
    }
}
