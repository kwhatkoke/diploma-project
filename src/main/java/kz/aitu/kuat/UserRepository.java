package kz.aitu.kuat;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAll();

    Iterable<User> findTopByOrderByIdDesc();
}
