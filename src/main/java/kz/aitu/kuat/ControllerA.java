package kz.aitu.kuat;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

@RestController
@RequestMapping("/a")
public class ControllerA {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private DiplomaService service;

    private static final String BASE_URL
            = "http://164.92.153.55:8081/";

    private static final String SERVICE_A = "serviceA";

    int count = 0;
    int countRetry = 0;
    int countRetryCB = 0;
    int countCB = 0;

    @GetMapping("/a")
    public String serviceA() {
        System.out.println("a method called " + count++ + " times at " + new Date());
        return service.callB(BASE_URL + "b");
    }

    @GetMapping("/circuit-breaker")
    @CircuitBreaker(name = SERVICE_A, fallbackMethod = "serviceAFallback")
    public String serviceA_circuit_breaker() {
        System.out.println("circuit-breaker method called " + countCB++ + " times at " + new Date());
        return service.callB(BASE_URL + "b");
    }

    @Retryable(value = RuntimeException.class, maxAttempts = 5, backoff = @Backoff(3000))
    @GetMapping("/retry")
    public String serviceA_retry() {
        System.out.println("retry method called " + countRetry++ + " times at " + new Date());
        return service.callB(BASE_URL + "b");
    }

    @GetMapping("/circuit-breaker-retry")
    @CircuitBreaker(name = SERVICE_A, fallbackMethod = "serviceAFallback")
    @Retryable(value = RuntimeException.class, maxAttempts = 3, backoff = @Backoff(3000))
    public String serviceA_circuit_breaker_retry() {
        System.out.println("circuit-breaker-retry method called " + countRetryCB++ + " times at " + new Date());
        return service.callB(BASE_URL + "b");
    }

    public String serviceAFallback(Exception e) {
        return "This is a fallback method for Service A";
    }
}
