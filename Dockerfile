FROM openjdk:11

WORKDIR /
ARG CI_PROJECT_NAME
ADD target/${CI_PROJECT_NAME}.jar app.jar

CMD [ "sh", "-c", "java -Xms128m -Xmx128m -jar /app.jar $JAVA_APP_ARGS" ]
